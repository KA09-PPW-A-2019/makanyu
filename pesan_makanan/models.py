from django.db import models
from menu.models import Pesan 

# Create your models here.
class PesanMakanan(models.Model):
	# hari, tanggal, jam, nama kegiatan, tempat, dan kategori
	nama = models.CharField(max_length=20) #buat nama di adyssa dan munculin .idnya
	jumlah_harga = models.PositiveIntegerField() #gu
	pesanan = models.ForeignKey(Pesan, on_delete=models.CASCADE, null=True)

	def __str__(self):
		return '{} {} {}'.format(self.nama, self.jumlah_harga, self.pesanan)