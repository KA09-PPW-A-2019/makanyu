from django.urls import path
from . import views

app_name = 'pesan_makanan'

urlpatterns = [
    path('', views.pesan_makanan, name='pesan_makanan'),
]