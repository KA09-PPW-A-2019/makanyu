from django.test import TestCase, Client
from menu.models import Pesan, MenuDipesan, Menu
from .models import PesanMakanan

# Create your tests here.
class PesanMakanTest(TestCase):
	# ada linknya namanya pesan_makanan
	def test_cek_nama_url(self):
		Menu.objects.create(name='Ayam',price=16000,image=None)
		pesanan = Pesan()
		pesanan.save()
		for key, value in [['menu_1', 12]]:
			if key[:5] == 'menu_' and value > 0:
				menu_dipesan = MenuDipesan(menu=Menu.objects.get(id=int(key[5:])),
					amount=value)
				menu_dipesan.save()
				pesanan.menu.add(menu_dipesan)
		response =self.client.get('/pesan_makanan/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('pesan_makanan/pesanMakanan.html')
	
	# ada tombol pesan
	def test_cek_ada_tombol_pesan(self):
		Menu.objects.create(name='Ayam',price=16000,image=None)
		pesanan = Pesan()
		pesanan.save()
		for key, value in [['menu_1', 12]]:
			if key[:5] == 'menu_' and value > 0:
				menu_dipesan = MenuDipesan(menu=Menu.objects.get(id=int(key[5:])),
					amount=value)
				menu_dipesan.save()
				pesanan.menu.add(menu_dipesan)
		response =self.client.get('/pesan_makanan/')
		self.assertIn('<input class=" btn btn-primary shadow" type="submit" value="Pesan">', response.content.decode())
	
	#ada pesanan di dalam html
	def test_cek_ada_pesanan(self):
		Menu.objects.create(name='Ayam',price=16000,image=None)
		pesanan = Pesan()
		pesanan.save()
		for key, value in [['menu_1', 12]]:
			if key[:5] == 'menu_' and value > 0:
				menu_dipesan = MenuDipesan(menu=Menu.objects.get(id=int(key[5:])),
					amount=value)
				menu_dipesan.save()
				pesanan.menu.add(menu_dipesan)
		response =self.client.get('/pesan_makanan/')
		self.assertIn("Ayam", response.content.decode())

	#ada tulisan harga
	def test_cek_ada_harga(self):
		Menu.objects.create(name='Ayam',price=16000,image=None)
		pesanan = Pesan()
		pesanan.save()
		for key, value in [['menu_1', 12]]:
			if key[:5] == 'menu_' and value > 0:
				menu_dipesan = MenuDipesan(menu=Menu.objects.get(id=int(key[5:])),
					amount=value)
				menu_dipesan.save()
				pesanan.menu.add(menu_dipesan)
		response =self.client.get('/pesan_makanan/')
		self.assertIn("Rp 16,000", response.content.decode())

	#ada jumlah harga
	def test_cek_jumlah_harga(self):
		Menu.objects.create(name='Ayam',price=16000,image=None)
		pesanan = Pesan()
		pesanan.save()
		for key, value in [['menu_1', 12]]:
			if key[:5] == 'menu_' and value > 0:
				menu_dipesan = MenuDipesan(menu=Menu.objects.get(id=int(key[5:])),
					amount=value)
				menu_dipesan.save()
				pesanan.menu.add(menu_dipesan)
		response =self.client.get('/pesan_makanan/')
		self.assertIn("Rp "+str(16*12)+",000", response.content.decode())

	#models dapat dicetak sesuai format
	def test_models_dicetak_dengan_benar(self):
		Menu.objects.create(name='Ayam',price=16000,image=None)
		pesanan = Pesan()
		pesanan.save()
		for key, value in [['menu_1', 12]]:
			if key[:5] == 'menu_' and value > 0:
				menu_dipesan = MenuDipesan(menu=Menu.objects.get(id=int(key[5:])),
					amount=value)
				menu_dipesan.save()
				pesanan.menu.add(menu_dipesan)
		p = PesanMakanan.objects.create(nama="Brian",jumlah_harga=16000*12,pesanan=pesanan)
		response =self.client.get('/pesan_makanan/')
		self.assertEquals("Brian "+str(16000*12)+" "+str(pesanan), str(p))

	#test form tidak valid
	def test_form_valid(self):
		Menu.objects.create(name='Ayam',price=16000,image=None)
		pesanan = Pesan()
		pesanan.save()
		for key, value in [['menu_1', 12]]:
			if key[:5] == 'menu_' and value > 0:
				menu_dipesan = MenuDipesan(menu=Menu.objects.get(id=int(key[5:])),
					amount=value)
				menu_dipesan.save()
				pesanan.menu.add(menu_dipesan)
		data = { 'nama' : "bambang" }
		response =self.client.post('/pesan_makanan/', data)
		self.assertEqual(response.status_code, 302)
	#apabila tombol pesan ditekan, menu makanan akan dimasukan ke database
	# def test_(self):
	# 	response = self.client.get('/pesan_makanan/')
	# 	self.assertIn('Pesanan Kamu', response.conntent.decode())
#ada div background putih BLM
	# def test_cek_ada_background_blur(self):
	# 	response = self.client.get('/pesan_makanan/')
	# 	self.assertIn('tombol_pesan">', response.conntent.decode())
#ada tampilan menu yang akan dipesan
	
#background halaman blur

#ada harga menu yang sudah dikali dengan jumlah pesanan

#ada harga total

#ada tulisan note di bawah menu makanan bila ada

#keterangan makanan berwarna abu-abu

#harus mengirim nama yg mesan jg ketika pencet pesan

