from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PesanMakanan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=20)),
                ('jumlah_makanan', models.PositiveIntegerField()),
                ('nama_makanan', models.CharField(max_length=100)),
                ('harga_makanan', models.PositiveIntegerField()),
                ('deskripsi', models.CharField(max_length=20)),
                ('jumlah_harga', models.PositiveIntegerField()),
            ],
        ),
    ]
