from django import forms
from .models import Restaurant

class RestaurantForm(forms.ModelForm):
    class Meta:
        model = Restaurant
        fields = ['nama', 'deskripsi', 'alamat', 'gambar']
        labels  = {
        'nama':'Nama Restoran', 
        'deskripsi':'Deskripsi Restoran', 
        'alamat':'Alamat Restoran', 
        'gambar':'Foto Restoran',
        }

        widgets = {
        'nama' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text', 'placeholder' : 'Rumah Makan Sederhana'}),
        'deskripsi' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text', 'placeholder' : 'Aneka Masakan Padang'}),
        'alamat' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text', 'placeholder' : 'Jl. Menuju Surga'}),

        }
    
        
