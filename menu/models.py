from django.db import models
# from django.contrib.auth.models import User
# Create your models here.
class Menu(models.Model):
	name = models.CharField(max_length=30)
	price = models.PositiveIntegerField()
	image = models.ImageField(upload_to='images/')

	def __str__(self):
		return self.name

class MenuDipesan(models.Model):
	menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
	amount = models.PositiveSmallIntegerField()

	def __str__(self):
		return str(self.menu)

class Pesan(models.Model):
    menu = models.ManyToManyField(MenuDipesan)