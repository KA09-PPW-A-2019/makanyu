from django import forms
from .models import Menu

class MenuForm(forms.ModelForm):
    class Meta:
        model = Menu
        fields = ['name', 'price','image']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class PesanForm(forms.Form):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		for menu in Menu.objects.all():
			self.fields['menu_' + str(menu.id)] = forms.IntegerField(
				min_value=0,
				max_value=32767,
				initial=0,
			)
