from django.test import TestCase, Client
from menu.forms import MenuForm, PesanForm
from menu.models import Pesan, MenuDipesan, Menu
from .models import Menu

# Create your tests here.
class MenuHTMLTest(TestCase):
    def test_form(self):
        response = self.client.get('')
        
        self.assertIn('<form', response.content.decode())
        self.assertIn('<input', response.content.decode())

    def test_menu_url_is_exist(self):
        response = self.client.get('/menu/menumakan/')

        self.assertEqual(response.status_code,200)

    def test_cek_title(self):
        response = self.client.get('/menu/menumakan/')

        self.assertContains(response, 'Ayam Penyet Strike')

class MenuTest(TestCase):
    def test_exist(self):
        response = self.client.get('')

        self.assertContains(response, 'Ayam Penyet Strike')

    def test_setup_menu(self):
        menu1 = Menu(
            name='Ayam Taliwang',
            price=22000,
            image='image/ayam-taliwang.jpg',
        )
        menu1.save()
        menu2 = Menu(
            name='Ayam Goreng Rica - Rica',
            price=16000,
            image='image/ayam-goreng-rica-rica.jpg',
        )
        menu2.save()
        menu3 = Menu(
            name='Ayam Goreng Mentega',
            price=16000,
            image='image/ayam-goreng-mentega.jpg',
        )
        menu3.save()

    def test_response_landing_page(self):
        self.response = Client().get('/menu/menumakan/')
        self.assertEqual(self.response.status_code, 200)


    # def test_menu_using_menumakan_template(self):
    #     response = self.client.get('/menu/menumakan/')
    #     self.assertTemplateUsed(response, menumakan.html)
        
    # def test_text_exist(self):
    #     response = self.client.get('')
    #     self.assertIn('Name:', response.content.decode())
    #     self.assertIn('Price:', response.content.decode())

    # def test_pesan_button(self):
    #     response = self.client.get('')
    #     self.assertIn('Memesan', response.content.decode())

# class MenuViewsTest(TestCase):
    # def test_form_valid(self):
    #     response = self.client.post('/testimonial/', data={
    #         'name' : 'Ayam Taliwang',
    #         'price' : '22000',
    #         'image' : 'menu/static/images/ayam-bakar-taliwang.jpg',
    #         })
    #     self.assertTrue(Menu.objects.filter(nama="Ayam Taliwang").exists())

# test css background

