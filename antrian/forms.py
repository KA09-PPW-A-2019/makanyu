from django import forms
from .models import antrians

class antrianForms(forms.ModelForm):
    class Meta:
        model = antrians
        fields = ['pemesan', 'pesanan']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
