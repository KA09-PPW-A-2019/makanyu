from .models import antrians
from . import forms
from django.shortcuts import render, redirect

def antrian(request):
    order = antrians.objects.all().order_by('pemesan')
    return render(request, 'antrian.html') #, {'order': order})

def antrian_create(request):
    form = forms.antrianForms(request.POST) 
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('antrian')

    else:
        form = forms.antrianForms()
        return render(request, 'antrian_create.html', {'form': form})

def antrian_clear(request):
    antrians.objects.all().delete()
    return render(request, "antrian.html")

def antrian_delete(request, id):
    Antrian = antrians.objects.get(id=id)
    Antrian.delete()
    return redirect('antrian')
