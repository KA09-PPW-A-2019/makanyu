from django.urls import path
from . import views

urlpatterns = [
    path('', views.antrian, name='antrian'),
    path('create/', views.antrian_create, name='antrian_create')
    ]
