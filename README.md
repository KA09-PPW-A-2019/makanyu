# Makanyu

[![pipeline status](https://gitlab.com/KA09-PPW-A-2019/makanyu/badges/master/pipeline.svg)](https://gitlab.com/KA09-PPW-A-2019/makanyu/commits/master)
[![coverage report](https://gitlab.com/KA09-PPW-A-2019/makanyu/badges/master/coverage.svg)](https://gitlab.com/KA09-PPW-A-2019/makanyu/commits/master)

Tugas Kelompok 1 KA09 - PPW 2019

## Pengenalan

- Herokuapp link	: [Makanyu](https://makanyu.herokuapp.com/)

- Anggota Kelompok **KA09**	:
	1. Adyssa Fairuz Husniyyah - 1806191156
	2. Amrisandha Pranantya Prasetyo - 1806205400
	3. Muhammad Nurkholish - 1806186881
	4. Wiena Amanda Surianingrat - 1806186591

## Tentang Makanyu

Makanyu adalah aplikasi yang dapat membantu pengguna dalam memesan makanan sehingga pengguna tidak perlu berdiri lama untuk mengantri dalam memesan dan menunggu makanan lagi. Latar belakang ide aplikasi ini didapat dari kantin Fasilkom yang selalu penuh pada saat jam makan siang, sehingga waktu istirahat siang elemen-elemen Fasilkom UI terhabiskan untuk berdiri mengantri lama untuk memesan dan menunggu makanan. Ide ini juga dapat diimplementasikan dalam tempat makan selain kantin Fasilkom UI.


## Fitur Website:

1. Search Engine Tempat Makan (Amrisandha Pranantya Prasetyo)
	Fitur ini berada di halaman utama. Di dalam fitur ini, pengguna dapat mencari tempat makan yang mereka inginkan melalui search engine tersebut.
2. ChatBot
	Fitur ini adalah tempat chat pengguna dengan ChatBot, bertujuan untuk merekomendasi tempat makan yang sesuai dengan permintaan jenis makanan pengguna.
3. Daftar Tempat Makan 
	Fitur ini menampilkan daftar semua tempat makan yang terhubung dengan Makanyu. Pengguna dapat memilih tempat makan yang diinginkan dari daftar tersebut.	
4. Menu Tempat Makan & Pemesanan Makanan (Wiena Amanda Surianingrat & Muhammad Nurkholish)
	Fitur ini menampilkan menu dari tempat makan yang dipilih oleh pengguna. Pengguna yang sudah login dapat memesan makanan di tempat makan tersebut dengan memencet tombol tambah di sebelah setiap makanan dalam menu.
5. Data Pengunjung dalam Antrian (Adyssa Fairuz Husniyyah)
	Fitur ini menampilkan data pengguna dalam antrian pemesanan makanan beserta status pemesanan dan estimasi waktu pemrosesan pesanan.

